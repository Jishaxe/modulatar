''' Manages user accounts and sqlite3 operations '''

import sqlite3 as sql
import traceback

accountsdb = "accounts.db"

def createDB(): # Set up the database
    conn = sql.connect(accountsdb)
    
    conn.execute("CREATE TABLE IF NOT EXISTS accounts(username TEXT PRIMARY KEY, password TEXT)") # Create the table
    conn.commit()
    conn.close()
        
class User: # Represents a user
    username = ""
    
    def login(self, username, password):
        conn = sql.connect(accountsdb)
        
        self.username = username
        
        cur = conn.cursor()
        statement = "SELECT * FROM accounts WHERE username='" + username + "' AND password='" + password + "'"
        cur.execute(statement)
        res = cur.fetchall()
        conn.close()
        
        if len(res) != 0:
            return True
        else:
            return False
    
    def register(self, username, password):
        conn = sql.connect(accountsdb)
        
        try:
            statement = "INSERT INTO accounts VALUES ('" + username + "', '" + password + "')"
            conn.execute(statement) # Insert the new account
            conn.commit()
            self.login(username, password) # Now log in
            return True
        except: # Failed to execute
            traceback.print_exc()
            return False
        
        conn.close()
        
    
    
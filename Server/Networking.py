''' Handles all networking aspects '''


import Account
import threading
import traceback
import socket
import time
import sys

class Client(threading.Thread):
    clientSocket = socket.socket()
    connected = False
    loggedin = False
    server = None
    user = None
    address = ""
    
    def __init__(self, server):
        threading.Thread.__init__(self)
        self.server = server
    
    def setup(self, sock, addr): # Call upon a connection
        self.clientSocket = sock
        self.address = addr
        self.connected = True
        print("Recieved connection from " + addr + ", waiting for authorization...")
        self.start() # Start recieving
        
    def sendCommand(self, command): # Send command to other side
        if self.connected == False:
            raise Exception("Tried to send without connection!")
        
        self.clientSocket.sendall(command + '\n')
        
    def disconnect(self): # Disconnect client
        if self.connected:
            self.sendCommand("DISCONNECT")
            self.clientSocket.close()
            self.connected = False
    
    def processmessage(self, msg): # Process a command
        com = msg.split(" ")
        
        if self.loggedin: # If the client is logged in
            if msg == "":
                self.sendCommand("ERR Empty command!")
            else:
                print("[" + self.user.username + "] " + msg)
        else: # If it isn't logged in, demand credentials
            if com[0] == "LOGIN": # Log in command
                if len(com) == 3: # Make sure it has arguments
                    username = com[1]
                    password = com[2]
                    
                    print("Client " + self.address + " is attempting to log in with username " + username + " and password " + password + "...")
                    self.user = Account.User()
                    
                    if self.user.login(username, password):
                        self.sendCommand("OK")
                        self.loggedin = True
                        print("User " + username + " logged in successfully!")
                        for client in self.server.clients:
                            if client != self and client.loggedin: # Notifier other clients of login
                                client.sendCommand("LOGIN " + self.user.username)
                        
                    else:
                        self.sendCommand("FAIL")
                        self.disconnect()
                        print("Client " + self.address + " failed to log in!")
                        
                else:
                    self.sendCommand("ERR Invalid arguments for LOGIN!")
                    
            elif com[0] == "REGISTER": # Create an account and log in with it
                if len(com) == 3:
                    username = com[1]
                    password = com[2]
                    
                    self.user = Account.User()
                    
                    if self.user.register(username, password):
                        self.sendCommand("OK")
                        print("Client " + self.address + " has registered an account under the username " + username)
                        self.loggedin = True
                    else:
                        self.sendCommand("FAIL")
                        print("Client " + self.address + " tried to register an existing account under the username " + username)
                        self.disconnect()
                else:
                    self.sendCommand("ERR Invalid arguments for REGISTER!")
            else:
                self.sendCommand("ERR You need to log in!")
            
            
    def run(self): # Recieve thread
        while self.connected:
            msg = ""
            msgend = False
            
            while msgend == False and self.connected:
                data = ""
                
                try:
                    data = self.clientSocket.recv(1)
                except:
                    self.connected = False
                
                if data:
                    if data == "\n":
                        self.processmessage(msg)
                        msgend = True
                    else:
                        msg = msg + data
                else:
                    self.connected = False
        
        self.clientSocket.close()
        if self.loggedin:
            print("User " + self.user.username + " disconnected!")
            for client in self.server.clients:
                if client != self and client.loggedin: # Notify other clients of logout
                    client.sendCommand("LOGOUT " + self.user.username)
        else:
            print("Client " + self.address + " disconnected without authorizing!")
                    
                    
            
        
        
        
    
    
class Server(threading.Thread):
    listenSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    listening = False
    version = ""
    clients = []
    
    def __init__(self, version):
        threading.Thread.__init__(self)
        self.version = version
    def begin(self):
        try:
            self.listenSocket.bind(("", 3336))
            self.listenSocket.listen(100)
            self.listening = True
            self.start()
        except:
            self.listenSocket.close()
            print("Couldn't set up server socket! Are you sure the port isn't in use?")
            sys.exit(1)
    
    def run(self):
        while self.listening == True:
            (sock, addr) = self.listenSocket.accept()
            client = Client(self)
            client.setup(sock, addr[0])
            client.sendCommand(self.version)
            self.clients.append(client)
        
    
    def shutdown(self):
        self.listening = False
        self.listenSocket.close()
        
        for client in clients:
            client.disconnect()

        
        
            
        
''' The server software for the Minecraft bot, Modulatar. Created by Josh Lee/Jishaxe '''

import Networking
import Account
version = "Modulatar Server 1.0"

if __name__ == '__main__':
    print(version)
    
    Account.createDB()
    serv = Networking.Server(version)
    serv.begin()